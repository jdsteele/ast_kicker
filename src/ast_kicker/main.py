import click


@click.group()
def cli():
    pass


@cli.command()
def graph():
    from .command.graph import command
    command()


def main():
    cli()
